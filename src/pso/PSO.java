package pso;

import java.util.LinkedList;

import main.Constant;

import cec.testfunc;

import odma.ImplementicProblems;


public class PSO {

	private static double npop=Constant.numOfSoftwares;
	private static int nvar=Constant.numOfDim;
	private static double w=1;
	private static double wdamp=.99;
	private static double c1=2;
	private static double c2=2;
	private static double xmin=Constant.lessBound;
	private static double xmax=Constant.highBound;
	private static double dx;
	private static double vmax;
	private static int maxit=1000;
	private static double[][] gbest;
	private static double[] gbestcost;
	public static ImplementicProblems fit;
	static testfunc fit_cec;
	int functionName;
	public PSO(){
		fit = new ImplementicProblems();
		fit_cec = new testfunc();
	}
	
	/*public static void main(String[] args) {
		PSO pso = new PSO();
		pso.start();
		
	}*/

	public void start(double xRandom[][],int functionName){
		this.functionName=functionName;
		dx=xmax-xmin;
		vmax=.1*dx;
		int convergeRate=10;
		LinkedList<Particle> particle = new LinkedList();
		
		gbest = new double[maxit][nvar];
		gbestcost= new double[maxit];
		
		for(int it=0;it<maxit;++it){
			if(it==0){
				gbestcost[0]=Double.MAX_VALUE;
				for(int i=0;i<npop;++i){
					particle.add(new Particle());
					particle.get(i).velocity=new double[nvar];
					double[] temp = new double[nvar];
					for(int k=0;k<nvar;++k){
						temp[k]=xRandom[i][k];
						
					}
					
					particle.get(i).position=temp;
					particle.get(i).cost=Cost(particle.get(i).position,this.functionName);
					particle.get(i).pbest=particle.get(i).position;
		            particle.get(i).pbestcost=particle.get(i).cost;
		            
		            if (particle.get(i).pbestcost<gbestcost[it]){
		                gbest[it]=particle.get(i).pbest;
		                gbestcost[it]=particle.get(i).pbestcost;
		            }
		            
				}
				if(it%convergeRate==0 || it==999){
					print(it);
					}	
			}else{
				for(int i=0;i<nvar;++i){
					gbest[it][i]=gbest[it-1][i];
				}
				gbestcost[it]=gbestcost[it-1];
				
				for(int i=0;i<npop;++i){
					
					particle.get(i).velocity=addMatrix3(numMultiply(w,particle.get(i).velocity),numMultiply(c1*Math.random(), subMatrix(particle.get(i).pbest,particle.get(i).position)),numMultiply(c2*Math.random(), subMatrix(gbest[it], particle.get(i).position)));
					
					particle.get(i).velocity=boundingV(particle.get(i).velocity);
					particle.get(i).position=addMatrix(particle.get(i).position,particle.get(i).velocity);
					particle.get(i).position=boundingP(particle.get(i).position);
					particle.get(i).cost=Cost(particle.get(i).position,this.functionName);
					
					if (particle.get(i).cost<particle.get(i).pbestcost){
		                particle.get(i).pbest=particle.get(i).position;
		                particle.get(i).pbestcost=particle.get(i).cost;
	
		                if (particle.get(i).pbestcost<gbestcost[it]){
		                	for(int j=0;j<nvar;++j){
		    					gbest[it][j]=particle.get(i).pbest[j];
		    				}
		                    gbestcost[it]=particle.get(i).pbestcost;
						}
					}
					
				}
				
			}
			w=w*wdamp;
			if(it%convergeRate==0 || it==999){
			print(it);
			}
			
		}
		
		//System.out.println();
		}
		
		
		
		
	
	private void print(int it) {
		System.out.print(gbestcost[it]+",");
		
	}

	private static double[] addMatrix3(double[] a,
			double[] b, double[] c) {
		double[] d = new double[a.length];		
		for(int i=0;i<a.length;++i){
			d[i]=a[i]+b[i]+c[i];
		}
		
		
		return d;
	}


	private static double[] subMatrix(double[] a, double[] b) {
		double[] c = new double[a.length];
		for(int i=0;i<a.length;++i){
			c[i]=a[i]-b[i];
		}
		return c;
	}


	private static double[] numMultiply(double a, double[] b) {
		double[] c = new double[b.length];
		for(int i=0;i<b.length;++i){
			c[i]=a*b[i];
		}
		return c;
	}


	private static double[] boundingP(double[] position) {
		for(int i=0;i<position.length;++i){
			if(position[i]>xmax){
				position[i]=xmax;
			}
			if(position[i]<xmin){
				position[i]=xmin;
			}
		}
		return position;
	}


	private static double[] addMatrix(double[] a, double[] b) {
		double[] c = new double[a.length];
		for(int i=0;i<a.length;++i){
			c[i]=a[i]+b[i];
		}
		return c;
	}


	private static double[] boundingV(double[] velocity) {
		for(int i=0;i<velocity.length;++i){
			if(velocity[i]>vmax){
				velocity[i]=vmax;
			}
			if(velocity[i]<-vmax){
				velocity[i]=-vmax;
			}
		}
		return velocity;
	}

	//fitnessCEC
	private static double Cost(double[] position,int functionName) {
		// TODO Auto-generated method stub
		double[] y= new double[1];
		/*try {
			fit_cec.test_func(position,y,Constant.numOfDim,1,functionName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		//y[0]=fit.sphere(position);
		//y[0]=fit.Griewank(position);
		/*for (int i=0;i<position.length;i++)
			y+=position[i]*position[i];*/
		y[0]=fit.dejoinNose(position);
		return y[0];
	}

}
