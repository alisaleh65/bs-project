package bazar;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

public class Algorithm {
	static int MAX_ITER = 500;
	static int POPULATION = 50;
	static int PROBLEM_DIMENSION = 50;
	static double LOWER_BOUND = -100;
	static double UPER_BOUND = 100;
	static double marketRadius = 2.5;
	static Person population[];
	static Person bestMarket;
	static double alpha = 0.05;
	static Person globalBest = new Person(new double[PROBLEM_DIMENSION]);
	static double GB = Double.MAX_VALUE;
	static int functionName = 7;
	public static void findOptimal() throws InterruptedException, IOException{
		
		globalBest.fitness = Double.MAX_VALUE;
		initPopulation();
		
		
		for (int i = 0; i < MAX_ITER; i++) {
			
//			Test.showData(population);
//			Thread.sleep(300);
			
			setNeighbors();
			updateAtraction();
			computeFitness();
			
			
			for (int j = 0; j < POPULATION; j++) {
				setNeighbors();
				updateAtraction();
				if(isSpecialist(population[j].getNeighbor(), population[j])){
					population[j].setLocalBest(true);
					specialistMove(j);
				}else{
					population[j].setLocalBest(false);
					normalMove(j);
				}
			}
			
			
			
//			writeFile(pw);
			
			
		}
		System.out.println(GB);
	}



	private static void setNeighbors() {
		for (int j = 0; j < POPULATION; j++) {
			Utility.getNeighbor(population, population[j], marketRadius);
		}
	}

	
	
	private static void writeFile(PrintWriter pw) {
		// TODO Auto-generated method stub
		for (int i = 0; i < population.length; i++) {
			
			pw.println(""+ population[i].position[0] + ";" + population[i].position[1]);
		}
	}



	private static void computeFitness() {
		// TODO Auto-generated method stub
		for (int i = 0; i < population.length; i++) {
			population[i].updateFitnessWithPosition(functionName);
			if(population[i].fitness < globalBest.fitness){
				globalBest = population[i];
				globalBest.setFitness(population[i].fitness);
			}
			if(population[i].fitness < GB){
				GB = population[i].fitness;
			}
		}
	}



	private static void normalMove(int j) {
		// TODO Auto-generated method stub

		double temp = Double.MAX_VALUE;
		int iTemp = -1;
		for (int i = 0; i < population.length; i++) {
			if(population[i].marketAttraction - population[j].marketAttraction < temp
					&& i != j){
				temp =  population[i].marketAttraction - population[j].marketAttraction;
				iTemp = i;
			}
		}
		double x[] = population[j].getPosition().clone();
		double attX = population[j].getMarketAttraction();
		
		if(temp/100 < Person.CHANGE_MARKET_T){
			
			
			x = Utility.sumVector(population[iTemp].position, 
					Utility.scalerProduct(Utility.normalizeVector(
								Utility.vectorDiff(population[j].position, population[iTemp].position)
							), 
							
							Math.min(1, population[iTemp].fitness/population[j].fitness*marketRadius)));
//			if(x[0] > 15 || x[1] > 15){
//				System.out.println("itemp = " + population[iTemp]);
//				System.out.println("j = " + population[j]);
//				System.out.println("zarib = " + (population[iTemp].fitness/population[j].fitness)*marketRadius);
//			}
			population[j].setPosition(x);
			
		}else{
//			Arrays.sort(population[j].neighbor);
			double maxDiffAndMax[] = Utility.findMaxDiffAndBestPopulationFit(population[j].neighbor);
			double maxDiff = maxDiffAndMax[0];
			double max = maxDiffAndMax[1];
			
			double nxtPosition[] = new double[population[j].position.length];
			double tt;
			for (int i = 0; i < population[j].neighbor.length; i++) {
				tt = (maxDiff - (max - population[j].neighbor[i].fitness))/maxDiff;
				nxtPosition = Utility.sumVector(nxtPosition, 
						Utility.scalerProduct(
								Utility.vectorDiff(x, population[j].neighbor[i].position)
								, tt) 
						);
			}
			nxtPosition = Utility.sumVector(x, nxtPosition);
			population[j].setPosition(nxtPosition);
		}
//		System.out.println(Arrays.toString(x));
	}



	private static double norm(double[] position) {
		// TODO Auto-generated method stub
		double result = 0;
		for (int i = 0; i < position.length; i++) {
			result += position[i]*position[i];
		}
		return Math.sqrt(result);
	}



	private static void specialistMove(int j) {
		// TODO Auto-generated method stub
//		spec++;
		double temp = Double.MAX_VALUE;
		int iTemp = -1;
		for (int i = 0; i < population.length; i++) {
			if(population[i].marketAttraction - population[j].marketAttraction < temp
					&& i != j){
				temp =  population[i].marketAttraction - population[j].marketAttraction;
				iTemp = i;
			}
		}
		double x[] = population[j].getPosition().clone();
		double attX = population[j].getMarketAttraction();
		
		if(temp/100 < Person.CHANGE_MARKET_T){
			
			
			x = Utility.sumVector(population[iTemp].position, 
					Utility.scalerProduct(Utility.normalizeVector(
								Utility.vectorDiff(population[j].position, population[iTemp].position)
							), 
							
							Math.min(1, population[iTemp].fitness/population[j].fitness*marketRadius)));
//			if(x[0] > 15 || x[1] > 15){
//				System.out.println("itemp = " + population[iTemp]);
//				System.out.println("j = " + population[j]);
//				System.out.println("zarib = " + (population[iTemp].fitness/population[j].fitness)*marketRadius);
//			}
			population[j].setPosition(x);
			
		}else{
			for (int i = 0; i < population[j].position.length; i++) {
				double[] greater = Utility.getGreaterThanInDimantionAVG(i, j);
				double[] smaller = Utility.getLessThanInDimantionAVG(i, j);
				if(greater[0] < smaller[0]){
					x[i] = greater[1];
				}else if(smaller[0] < greater[0]){
					x[i] = smaller[1];
				}else{
					
				}
			}
			population[j].setPosition(x);
		}
	}



	private static void updateAtraction() {
		// TODO Auto-generated method stub
		double att = Double.MAX_VALUE, temp;
		for (int i = 0; i < population.length; i++) {
			temp = attraction(population[i]);
			if( temp < att ){
				att = temp;
				bestMarket = population[i];
			}
		}
		
	}



	private static boolean isSpecialist(Person[] neighbor, Person person) {
		// TODO Auto-generated method stub
		boolean result = true;
		for (int i = 0; i < neighbor.length; i++) {
			
			if(neighbor[i].fitness < person.fitness){
				result = false;
			}
		}
		return result;
	}



	public static void initPopulation() {
		population = new Person[POPULATION];
		for (int i = 0; i < population.length; i++) {
			population[i] = new Person( 
					Utility.getRandomPosition(PROBLEM_DIMENSION, LOWER_BOUND, UPER_BOUND));
		}
	}
	
	public static double attraction(Person p){
		double result = 0;
		double PR = 0;
		Person n[] = p.getNeighbor();
		for (int i = 0; i < n.length; i++) {
			PR += n[i].fitness;
		} 
		
		PR = PR/n.length;
		double RR = n.length/marketRadius;
		result = p.fitness + PR + RR;
		p.setMarketAttraction(result);
		return result;
	}
	
	
}
