package bazar;
import java.util.Arrays;

public class Person implements Comparable<Person>{
	static final double CHANGE_MARKET_T = -1;
	static final double EXPERT_CHANGE_MARKET_T = -2;
	final int NUM_OF_HISTORY = 5;
	double position[];
	public void setPosition(double[] position) {
		this.position = position;
	}

	double fitness;
	boolean localBest;
	double marketAttraction;
	
	
	public double updateFitnessWithPosition(int functionName){
		this.fitness = Fitness.fitness(this.position, functionName);
		return this.fitness;
	}
	
	public double getMarketAttraction() {
		return marketAttraction;
	}

	public void setMarketAttraction(double marketAttraction) {
		this.marketAttraction = marketAttraction;
	}

	public boolean isLocalBest() {
		return localBest;
	}

	public void setLocalBest(boolean localBest) {
		this.localBest = localBest;
	}

	double lastPosition[][];
	
	Person neighbor[];
	
	public Person(double p[]){
		position = p;
		lastPosition = new double[NUM_OF_HISTORY][p.length];
	}
	
	public Person(double p[], double fit){
		this(p);
		fitness = fit;
	}

	public double[] getPosition() {
		// TODO Auto-generated method stub
		return position;
	}

	public void setNeighbor(Person[] array) {
		// TODO Auto-generated method stub
		neighbor = array;
	}

	public Person[] getNeighbor() {
		// TODO Auto-generated method stub
		return neighbor;
	}

	public void setFitness(double fitness2) {
		// TODO Auto-generated method stub
		fitness = fitness2;
	}

	@Override
	public int compareTo(Person arg0) {
		// TODO Auto-generated method stub
		return new Double(this.fitness).compareTo(arg0.fitness);
	}
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("{\nperson location = " + Arrays.toString(position)+"\nneighbors:\n");
		for (int i = 0; i < neighbor.length; i++) {
			sb.append(Arrays.toString(neighbor[i].position)+"\n");
		}
		sb.append("}");
		return sb.toString();
	}
}
