package bazar;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class Test {
	static ChartFrame frame;
	static XYSeriesCollection result = new XYSeriesCollection();
	static XYSeries series = new XYSeries("Population");
	static JFreeChart chart;
	static int chartCount = 0;
	public static void main(String[] args) throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		initChart();
		Algorithm.findOptimal();

	}

	private static void initChart() {
		frame = new ChartFrame("First", null);
		chart = ChartFactory.createScatterPlot("Scatter Plot", // chart
																			// title
				"X", // x axis label
				"Y", // y axis label
				result, // data ***-----PROBLEM------***
				PlotOrientation.VERTICAL, true, // include legend
				true, // tooltips
				false // urls
		);
		frame = new ChartFrame("First", chart);

		frame.pack();
		frame.setVisible(true);

		result.addSeries(series);
	}

	public static void showData(Person population[]) throws IOException {

		// print array in rectangular form
		// for (int r = 0; r < a2.length; r++) {
		// for (int c = 0; c < a2[r].length; c++) {
		// System.out.print(" " + a2[r][c]);
		// }
		// System.out.println("");
		// }

		series.clear();
		for (int i = 0; i < population.length; i++) {
			double x = population[i].position[0];
			double y = population[i].position[1];
			series.add(x, y);
		}
		++chartCount;
//		File f = new File("img/"+chartCount+".jpeg");
//		ChartUtilities.saveChartAsJPEG(f, chart, 300, 300);
		
		// create a chart...

		// create and display a frame...

	}

}
