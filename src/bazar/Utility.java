package bazar;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;

import javax.swing.Popup;

public class Utility {
	static Random r = new Random(System.currentTimeMillis());
	
	public static double[] getRandomPosition(int dimention, double lower, double uper){
		double result[] = new double[dimention];
		for (int i = 0; i < result.length; i++) {
			result[i] = lower + r.nextDouble()*(uper-lower);
		}
		return result;
	}
	
	
	public static Person[] getNeighbor(Person population[], Person p, double radius){
		LinkedList<Person> result = new LinkedList<Person>();
		for (int i = 0; i < population.length; i++) {
			if(distance(population[i], p) < radius){
				result.add(population[i]);
			}
		}
			
		Object tempObj[] = result.toArray();
		Person prsns[] = new Person[tempObj.length];
		for (int i = 0; i < tempObj.length; i++) {
			prsns[i] = (Person)tempObj[i];
		}
		
		p.setNeighbor( prsns );
		return p.getNeighbor();
	}


	static double distance(Person person, Person p) {
		double result = 0;
		double a[] = person.getPosition();
		double b[] = p.getPosition();
		for (int i = 0; i < a.length; i++) {
			result += (a[i]-b[i])*(a[i]-b[i]);
		}
		return Math.sqrt(result);
	}
	
	static double[] vectorDiff(double v1[], double v2[]){
		if(v1.length != v2.length)
			return null;
		double result[] = new double[v1.length];
		for (int i = 0; i < v2.length; i++) {
			result[i] = v2[i] - v1[i];
		}
		return result;
	}
	
	static double[] scalerProduct(double v[], double alpha){
		double result[] = new double[v.length];
		for (int i = 0; i < result.length; i++) {
			result[i] = alpha*v[i];
		}
		return result;
	}
	
	static double[] sumVector(double v1[], double v2[]){
		if(v1.length != v2.length)
			return null;
		double result[] = new double[v1.length];
		for (int i = 0; i < v2.length; i++) {
			result[i] = v2[i] + v1[i];
		}
		return result;
	}
	
	
	static double[] normalizeVector(double[] v){
		double temp = 0;
		for (int i = 0; i < v.length; i++) {
			temp += v[i]*v[i];
		}
		temp = Math.sqrt(temp);
		for (int i = 0; i < v.length; i++) {
			v[i] /= temp;
		}
		return v;
	}


	public static double[] getLessThanInDimantionAVG(int i, int p) {
		double result[] = new double[2];
		int smallerCount = 0;
		for (int j = 0; j < Algorithm.population.length; j++) {
			if(Algorithm.population[j].position[i] < Algorithm.population[p].position[i]){
				result[0] += Algorithm.population[j].fitness;
				smallerCount++;
				result[1] += Algorithm.population[j].position[i];
			}
		}
		result[0] = result[0]/Algorithm.population.length;
		result[1] = result[1]/smallerCount;
		return result;
	}


	public static double[] getGreaterThanInDimantionAVG(int i, int p) {
		double result[] = new double[2];
		int bigerCount = 0;
		for (int j = 0; j < Algorithm.population.length; j++) {
			if(Algorithm.population[j].position[i] > Algorithm.population[p].position[i]){
				result[0] += Algorithm.population[j].fitness;
				bigerCount++;
				result[1] += Algorithm.population[j].position[i];
			}
		}
		result[0] = result[0]/Algorithm.population.length;
		result[1] = result[1]/bigerCount;
		
		
		
		return result;
	}


	public static double[] findMaxDiffAndBestPopulationFit(Person neighbors[]) {
		// TODO Auto-generated method stub
		double result[] = new double[2];
		double max = Double.MIN_VALUE;
		double min = Double.MAX_VALUE;
		for (int i = 0; i < neighbors.length; i++) {
			if(neighbors[i].fitness < min){
				min = neighbors[i].fitness;
			}
			if(neighbors[i].fitness > max){
				max = neighbors[i].fitness;
			}
		}
		result[0] = max - min;
		result[1] = max;
		return result;
	}

	
}
