package main;

public class Constant {
	//number of initial population
	public static int numOfSoftwares=50;
	public static int numOfweakSoftwares=25;
	public static int numOfSubSoft= 25 ;
	//dimension
	public static int numOfDim=50;
	public static double dobNumOfDim=numOfDim;
	//The bound of function.
	public static double lessBound=-100;
	public static double highBound=100;
	//number of iteration.
	public static int iterations=500;
	//Constant for divide radius of create new softwares. 
	public static int constant=2;
	
	public static int numOfTopMovment=5;
	public static int numOfTopSoftware=5;
	public static double a=0.5;
	public static int divide = 10;
	public static double radiousMovement=highBound/divide;
	public static String funcName;
}
