package main;

import ica.TestICA;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Random;

import bazar.Algorithm;
import main.Constant;
import odma.ODMA;
import pso.PSO;

public class Start {
	
	private static  PrintStream p;
	static int numOfRun;
	public static void main(String[] args) throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		int functionName[] = {7};
		numOfRun=15;
		
		for(int func =0;func<functionName.length;func++){
			double bestODMA[]= new double[numOfRun];
			double bestPSO[]= new double[numOfRun];
			double bestICA[]= new double[numOfRun];
			try {
				p = new PrintStream(new File("output_"+functionName[func]+".csv"));
//				System.setOut(p);///////////////////////////////////////////////////
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			for(int p=0;p<numOfRun;p++){
				double xRandom[][]=new double[Constant.numOfSoftwares][Constant.numOfDim];
				Random rn = new Random();
				for(int i=0;i<Constant.numOfSoftwares;i++){
			
					for(int j=0;j<Constant.numOfDim;j++){
				
						//Create random software in 
						xRandom[i][j]=(Constant.highBound - Constant.lessBound)*rn.nextDouble()+ Constant.lessBound;
					}
				}
			
				System.out.print("ODMA,");	
				ODMA odma = new ODMA(xRandom,functionName[func]);
				odma.start();
				System.out.println();
				
//				bazar.Algorithm.findOptimal();
//				
//				System.out.println();
				
//				System.out.print("PSO,");	
//				PSO pso = new PSO();
//				pso.start(xRandom,functionName[func]);
//				System.out.print("ICA,");
//				TestICA ica = new TestICA();
//				ica.start(args, functionName[func]);
//				bestICA[p]=ica.start(args, functionName[func]);
//				System.out.println();
				}
				
			
			System.out.println();
			System.out.println("***********************************");
			//print(bestICA,"ICA");
		}
			
		
		
	
	}
	private static void print(double best[], String name) {
		// TODO Auto-generated method stub
		System.out.println(name+"\n");
		for(int i=0;i<numOfRun;i++){
			System.out.println(best[i]);
		}
	}
}
