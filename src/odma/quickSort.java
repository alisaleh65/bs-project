package odma;

import java.util.Scanner;

import main.Constant;



public class quickSort {

	private listSoftware[] element;//=new listSoftware[Constant.numOfSoftwares];
	
	public listSoftware[] quickSort(listSoftware[] softwares){
		
		element=softwares;
//		quickSort(0,element.length - 1 );
		for(int i=0;i<element.length;i++){
			for(int j=0;j<element.length;j++){
				if(element[i].getySoftware()<element[j].getySoftware()){
					listSoftware temp=element[j];
					element[j]=element[i];
					element[i]=temp;
				}
			}
		}
		return element;
	}
	
     public void quickSort(int s,int e){
		
		if(s<e){
			
			int q=partition(s,e);
			element[q].setPoint(Constant.numOfSoftwares-q);
			quickSort(s,q-1);
			quickSort(q+1,e);
			
			
		}
		
	}
	
	public int partition(int s,int e){
		
		double x=element[e].getySoftware();
		int i=s-1;
		
		for(int j=s;j<e;j++){
			
			if(element[j].getySoftware()<=x){
				
				i++;
				
				swap(i,j);
			}
		}
		
		i++;
		if(element[i].getySoftware()<=element[e].getySoftware())
		swap(i,e);	
		
		return i;
	}
	public void swap(int i,int j){
		
		listSoftware temp=element[j];
		element[j]=element[i];
		element[i]=temp;
	}
}
