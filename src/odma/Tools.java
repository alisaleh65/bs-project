package odma;

import main.Constant;

public class Tools {
	
	public double[] log(int number,int itr){
			double x[]= new double[number];
		 	int maxN = (int) Constant.highBound/itr;
		    int t = (int) Math.pow(2, maxN); // 2^maxN
		    for(int i=0;i<number;i++){
		    double n =  (Math.log((Math.random() * t) + 1)/Math.log(2));
		    x[i]=(Constant.highBound/itr)-n;
		   
		    
		    }
		    
		    return x;
	}
	
	public double[] normal(int number,int itr){
		double itr2=5*itr/Constant.iterations;
		double normal =Constant.highBound*(0.2+((1/(Math.sqrt(5)*Math.sqrt(2*Math.PI)))*Math.exp(-(Math.pow(itr2-0, 2)/2*5))));
		
		double[] x = new double[number];
		for(int i=0;i<number;i++){
			x[i] = Math.random()*normal;
			
			  }
		
		return x;
	}

}
