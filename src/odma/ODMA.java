package odma;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Random;

import main.Constant;
import cec.*;

public class ODMA {

	/**
	 * Write By Hossein Hajipour.
	 */
	public static ODMA odma;
	//print results on a file
	private static PrintStream p;
	//number of initial population
	private int numOfSoftwares;
	//number of new software based on leading software.
	private int numOfSubSoftware;
	//array of all softwares
	private listSoftware listSoftwares[];
	//one software
	private listSoftware software;
	//dimension
	private int numOfDim;
	//create object of Random Class
	private Random rn = new Random();
	//object for sorting elements
	private quickSort sort;
	double CM;
	private ImplementicProblems fit;
	private Numerical_10_5_8_1_ContinuousLogSigmoid fit2;
	private Tools tools;
	private int numOfTop;
	int itr;
	testfunc fit_cec;
	int functionName;
	public ODMA(double xRandom[][],int functionName){
		this.functionName=functionName;
		this.numOfSoftwares=Constant.numOfSoftwares;
		this.numOfDim=Constant.numOfDim;
		this.numOfSubSoftware=Constant.numOfSubSoft;
		this.numOfTop=Constant.numOfTopSoftware;
		this.itr=0;
		fit_cec = new testfunc();
		
		listSoftwares=new listSoftware[numOfSoftwares];
		sort=new quickSort();
		tools = new Tools();
		//start create incipient software
		for(int i=0;i<numOfSoftwares;i++){
			
			/*double xRandom[]=new double[numOfDim];
			
			for(int j=0;j<numOfDim;j++){
				
				//Create random software in 
				xRandom[j]=(Constant.highBound - Constant.lessBound)*rn.nextDouble()+ Constant.lessBound;
				
				
			}*/
			
			//Get value of function with X random software.
			double value=fitness(xRandom[i]);
			//Add software with details in special link list.
			listSoftwares[i]=new listSoftware(xRandom[i],value,1);
		}
		//Sort List by value.
		listSoftwares=sort.quickSort(listSoftwares);
		//print();
	
	}
	
	public double start(){
		//for check runtime.
		//System.out.println(System.currentTimeMillis());
		//System.out.println(listSoftwares.length);
		int convergeRate=5;
		for(int i=1; i<= Constant.iterations ; i++){
			//while(MiddleClass.getExit()==true);
			
			//In this method softwares try to get better position.
			tryToBetter();
			
			//Move leading software on linear equation of their history.
			moveTopSoftwares(i);
			if(i>1 && i%3==0){
			delet();
			}
			//for change behavior after a long time! 
			if(i%(Constant.iterations/2)==0){
				Constant.constant=2;
				Constant.radiousMovement=2;
			}
			//for change number of leading after a long time!
			if(i%(Constant.iterations/3)==0){
				numOfTop--;
			}
			
			//Create new software based on leading software.
			if(i>1 && i%3==0){
				createSubSoftwares();//createSubSoftware2(i);
			}
			if(i%(10)==0){
				//refactor();
			}
//			if(i%10==0){
//				App.mainPage.setvalue();
//			}
			//try{Thread.sleep(200);}catch(Exception e){}
			//App.mainPage.outPut(listSoftwares);
			itr=i;
			
			//System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			if(i%convergeRate==0 || i==499){
				//print();
			}
		}
		print();
		//System.out.println();
		
		//App.mainPage.printrRST(listSoftwares);
		//for check runtime.
		//System.out.println(System.currentTimeMillis());
		//print();
		
		 //print();
		return listSoftwares[0].getySoftware();
		
	}
	
	public void tryToBetter(){
		double counter=0;
		for(int f=0;f<numOfTop;f++){
			//Sum of all point of leading software ( point in paper is counter!)
			counter+=listSoftwares[f].point2;
		}
		
		for(int i=Constant.numOfTopSoftware;i<numOfSoftwares;i++){
			//Equation 1
			int topNum;
			//point2 in paper defined as counter. this statement can be extend
			topNum = (int) ((listSoftwares[i].point2/counter)*10);
			
			if(topNum>this.numOfTop){
				topNum=Constant.numOfTopSoftware;
			}		
			int num = Constant.numOfTopSoftware-topNum;
			int topNum2;
			if(num<=0){
				topNum2=0;
			}else if (num > Constant.numOfTopSoftware){
				topNum2 = Constant.numOfTopSoftware-1;
			}
			else{
			//System.out.println(num);
			//get a number between zero to num.
			 topNum2 = rn.nextInt(num);
			}
			//End of Equation
			
			//Start Equation 2
			listSoftware topSoftware=listSoftwares[topNum2];
			double[] bestX=new double[numOfDim];
			double bestY=Double.MAX_VALUE;
			// get random theta between zero to PI/4. 
			double Radian=((rn.nextDouble())*(Math.PI/4));
			// get first random number between zero to one!
			
			
				double xRandom[]=new double[numOfDim];
				// get tempX for store generated X!.
				double tempX[]=new double[numOfDim];
				double norm = 5*(((1/(Math.sqrt(5)*Math.sqrt(2*Math.PI)))*Math.exp(-(Math.pow(itr-0, 2)/2*5))));
				double rn2=rn.nextDouble();
				double rn1=rn.nextDouble();
				//Chaostic function
				CM = 1;
				// In this loop software try to get better positions.
				for(int k=0;k<numOfDim;k++){
					
					xRandom[k]=((((listSoftwares[i].point2/topSoftware.point2 + rn1) +rn2)
					           *(Math.abs(listSoftwares[i].getXSoftware()[k] - topSoftware.getXSoftware()[k])))/Math.cos(Radian));
					// in these condition statements check high and less bound of new postions.
					if(listSoftwares[i].getXSoftware()[k]>topSoftware.getXSoftware()[k]){
						  if((listSoftwares[i].getXSoftware()[k] - xRandom[k])>=Constant.lessBound && (listSoftwares[i].getXSoftware()[k] - xRandom[k])<=Constant.highBound){
						  tempX[k]= listSoftwares[i].getXSoftware()[k]- xRandom[k];
						  }else{
							  tempX[k]= Constant.lessBound;
						  }
					  }else{
						  if((listSoftwares[i].getXSoftware()[k] + xRandom[k]) <= Constant.highBound && (listSoftwares[i].getXSoftware()[k] + xRandom[k])>= Constant.lessBound){
							  tempX[k]= listSoftwares[i].getXSoftware()[k] + xRandom[k];
						  }else{
							  tempX[k]=Constant.highBound;
						  }
					  }//end if
				}
				double y=fitness(tempX);
				//Increase point of leading software and this software if get better position.
				if(y<listSoftwares[i].getySoftware()){
					listSoftwares[i].point2++;
					topSoftware.point2++;
				}
				
				
			//Update List.
			listSoftwares[i].setPreXSoftware(listSoftwares[i].getXSoftware());
			listSoftwares[i].setPreYSoftware(listSoftwares[i].getySoftware());
			listSoftwares[i].setYSoftware(y);
			listSoftwares[i].setXSoftware(tempX);
			listSoftwares[i].setSecondMovement(true);
			xRandom=null;
			tempX=null;
		}
		listSoftwares=sort.quickSort(listSoftwares);
		
	}
	
	public void moveTopSoftwares(int itr){
		
		for(int i=0;i<Constant.numOfTopSoftware;i++){
			
			if(listSoftwares[i].getSecondMovement()==true){
				
				for(int j=0;j<Constant.numOfTopMovment;j++){
				double[] tempX=new double[numOfDim];
				//Create linear equation.
				listSoftwares[i].creatVectorConductor();
				tempX=listSoftwares[i].creatLinerEquation(itr);
				//End of create process of linear equation
				
				double y=fitness(tempX);
				
				if(y<listSoftwares[i].getySoftware()){
					
					
					listSoftwares[i].setPreXSoftware(listSoftwares[i].getXSoftware());
					listSoftwares[i].setPreYSoftware(listSoftwares[i].getySoftware());
					listSoftwares[i].setYSoftware(y);
					listSoftwares[i].setXSoftware(tempX);
					
				}
				double Radian=((rn.nextDouble())*(Math.PI/4));
				for(int k=0;k<tempX.length;k++){
					tempX[k] = tempX[k]/Math.cos(Radian);
				}
				y=fitness(tempX);
				
				if(y<listSoftwares[i].getySoftware()){
					
					listSoftwares[i].setPreXSoftware(listSoftwares[i].getXSoftware());
					listSoftwares[i].setPreYSoftware(listSoftwares[i].getySoftware());
					listSoftwares[i].setYSoftware(y);
					listSoftwares[i].setXSoftware(tempX);
					
				}
				}
				//divide the radius of movement. 
				Constant.radiousMovement=Constant.radiousMovement/Constant.divide;
			}
		}
		
		listSoftwares=sort.quickSort(listSoftwares);
	}
	//Delete weak software
	public void delet(){
		
		for(int i=Constant.numOfSoftwares-1;i>=(Constant.numOfSoftwares/2);i--){
			listSoftwares[i]=null;
		}
	}
	
	public void createSubSoftware(){
		
		double Radious=Constant.highBound/Constant.constant;
		//Increase constant value to divide radius.
		Constant.constant++;
		int counter=Constant.numOfweakSoftwares;
		int newChild=0;
		for(int i=0;i<Constant.numOfTopSoftware;i++){
			//create new softwares based on leading softwares. 
			for(int j=0;j<Constant.numOfSubSoft;j++){
				newChild++;
				double[] xRandom=new double[numOfDim];
				double[] rightRadius=new double[numOfDim];
				double[] leftRadius=new double[numOfDim];
				
				for(int s=0;s<numOfDim;s++){
					leftRadius[s]=(listSoftwares[i].getXSoftware()[s]-Radious);
					rightRadius[s]=(listSoftwares[i].getXSoftware()[s]+Radious);
				}
				
				for(int k=0;k<numOfDim;k++){
					if(rightRadius[k] <= Constant.highBound && leftRadius[k]>= Constant.lessBound){
					xRandom[k] =leftRadius[k]+rn.nextDouble()*(rightRadius[k]-leftRadius[k]);
					}
					else if(leftRadius[k]>= Constant.lessBound){
						xRandom[k] = leftRadius[k]+rn.nextDouble()*(Constant.highBound-leftRadius[k]);
					}
					else{
						xRandom[k] = Constant.lessBound+rn.nextDouble()*(rightRadius[k]-Constant.lessBound);
					}
					
					}
				double y=fitness(xRandom);
				listSoftwares[counter]=new listSoftware(xRandom,y,-1);
				if(y<listSoftwares[i].getySoftware()){
					
					listSoftwares[counter].point2++;
					listSoftwares[i].point2+=listSoftwares[counter].point2;
				}
				counter++;
			}
			Constant.numOfSubSoft--;
			if(Constant.numOfSubSoft<=0)
				Constant.numOfSubSoft=1;
		}
		Constant.numOfSubSoft=7;
		sort.quickSort(listSoftwares);
	}
	
	
	public void createSubSoftware2(int itr){
		
		//double Radious=Constant.highBound/Constant.constant;
		//Increase constant value to divide radius.
		int counter=Constant.numOfweakSoftwares;
		Constant.constant++;
		
		for(int i=0;i<Constant.numOfTopSoftware;i++){
			//create new softwares based on leading softwares. 
			numOfSubSoftware=(int) ((Constant.numOfTopSoftware-i)*((double)(Constant.numOfweakSoftwares)/
												(double)(Constant.numOfTopSoftware*(Constant.numOfTopSoftware+1)/2)));
			double[] Radious = new double[numOfSubSoftware];
			Radious = tools.normal(numOfSubSoftware,itr);
			
			
			for(int j=0;j<numOfSubSoftware;j++){
				double[] xRandom=new double[numOfDim];
				double[] rightRadius=new double[numOfDim];
				double[] leftRadius=new double[numOfDim];
				
				for(int s=0;s<numOfDim;s++){
					leftRadius[s]=(listSoftwares[i].getXSoftware()[s]-Radious[j]);
					rightRadius[s]=(listSoftwares[i].getXSoftware()[s]+Radious[j]);
				}
				for(int k=0;k<numOfDim;k++){
					if(rightRadius[k] <= Constant.highBound && leftRadius[k]>= Constant.lessBound){
					xRandom[k] =leftRadius[k]+rn.nextDouble()*(rightRadius[k]-leftRadius[k]);
					}
					else if(leftRadius[k]>= Constant.lessBound){
						xRandom[k] = leftRadius[k]+rn.nextDouble()*(Constant.highBound-leftRadius[k]);
					}
					else{
						xRandom[k] = Constant.lessBound+rn.nextDouble()*(rightRadius[k]-Constant.lessBound);
					}
					
					}
				double y=fitness(xRandom);
				listSoftwares[counter]=new listSoftware(xRandom,y,1);
				if(y<listSoftwares[i].getySoftware()){
					
					listSoftwares[counter].point2++;
					listSoftwares[i].point2+=listSoftwares[counter].point2;
				}
				counter++;
			}
			
			if(Constant.numOfSubSoft<=0)
				Constant.numOfSubSoft=1;
		}
		if(counter<Constant.numOfSoftwares){
			
			
			double[] Radious = new double[Constant.numOfSoftwares-counter];
			Radious = tools.normal(Constant.numOfSoftwares-counter,itr);

			for(int l=0;l<=Constant.numOfSoftwares-counter;l++){
				
				
				
				double[] xRandom=new double[numOfDim];
				double[] rightRadius=new double[numOfDim];
				double[] leftRadius=new double[numOfDim];
				
				for(int s=0;s<numOfDim;s++){
					leftRadius[s]=(listSoftwares[0].getXSoftware()[s]-Radious[l]);
					rightRadius[s]=(listSoftwares[0].getXSoftware()[s]+Radious[l]);
				}
				
				for(int k=0;k<numOfDim;k++){
					if(rightRadius[k] <= Constant.highBound && leftRadius[k]>= Constant.lessBound){
					xRandom[k] =leftRadius[k]+rn.nextDouble()*(rightRadius[k]-leftRadius[k]);
					}
					else if(leftRadius[k]>= Constant.lessBound){
						xRandom[k] = leftRadius[k]+rn.nextDouble()*(Constant.highBound-leftRadius[k]);
					}
					else{
						xRandom[k] = Constant.lessBound+rn.nextDouble()*(rightRadius[k]-Constant.lessBound);
					}
					
					}
				double y=fitness(xRandom);
				listSoftwares[counter]=new listSoftware(xRandom,y,-1);
				if(y<listSoftwares[3].getySoftware()){
					
					listSoftwares[counter].point2++;
					listSoftwares[0].point2+=listSoftwares[counter].point2;
				}
			counter++;
			}
		}
		
		sort.quickSort(listSoftwares);
		
	}
	
	public void createSubSoftware3(int itr){
		int counter=Constant.numOfweakSoftwares;
		for(int i=0;i<Constant.numOfTopSoftware;i++){
			numOfSubSoftware=5;
			double[] Radious = new double[numOfSubSoftware];
			//Radious = //tools.log(numOfSubSoftware,itr);
			for(int m=0;m<Radious.length;m++){
				Radious[m]= Constant.highBound*Math.random()/itr;
				//System.out.println(Radious[m]);
			}
			//System.out.println("*************");
			for(int j=0;j<numOfSubSoftware;j++){
					double[] xRandom=new double[numOfDim];
					double[] rightRadius=new double[numOfDim];
					double[] leftRadius=new double[numOfDim];

					for(int s=0;s<numOfDim;s++){
						leftRadius[s]=(listSoftwares[i].getXSoftware()[s]-Radious[j]);
						rightRadius[s]=(listSoftwares[i].getXSoftware()[s]+Radious[j]);
					}

					for(int k=0;k<numOfDim;k++){
						if(rightRadius[k] <= Constant.highBound && leftRadius[k]>= Constant.lessBound){
							xRandom[k] =leftRadius[k]+rn.nextDouble()*(rightRadius[k]-leftRadius[k]);
						}
						else if(leftRadius[k]>= Constant.lessBound){
							xRandom[k] = leftRadius[k]+rn.nextDouble()*(Constant.highBound-leftRadius[k]);
						}
						else{
							xRandom[k] = Constant.lessBound+rn.nextDouble()*(rightRadius[k]-Constant.lessBound);
						}

					}
					double y=fitness(xRandom);
					listSoftwares[counter]=new listSoftware(xRandom,y,1);
					if(y<listSoftwares[i].getySoftware()){

						listSoftwares[counter].point2++;
						listSoftwares[i].point2+=listSoftwares[counter].point2;
					}
					counter++;
			}



		}
		
	}
	public void refactor(){
		double xRandom[]=new double[Constant.numOfDim];
		// get tempX for store generated X!.
		double tempX[]=new double[Constant.numOfDim];
	for(int i=1;i<10;i++){
		for(int k=0;k<Constant.numOfDim;k++){
			xRandom[k]=-10+rn.nextDouble()*20;
			// in these condition statements check high and less bound of new postions.
			
				  tempX[k]= listSoftwares[0].getXSoftware()[k]- xRandom[k];
				  if(tempX[k]>Constant.highBound){
					  tempX[k]=Constant.highBound;
				  }else if(tempX[k]<Constant.lessBound){
					  tempX[k]=Constant.lessBound;
				  }
			  
		}
		double y=fitness(tempX);
		//Increase point of leading software and this software if get better position.
		if(y<listSoftwares[i].getySoftware()){
			listSoftwares[i].point2++;
			//topSoftware.point2++;
		}
		
		
	//Update List.
	listSoftwares[i].setPreXSoftware(listSoftwares[i].getXSoftware());
	listSoftwares[i].setPreYSoftware(listSoftwares[i].getySoftware());
	listSoftwares[i].setYSoftware(y);
	listSoftwares[i].setXSoftware(tempX);
	listSoftwares[i].setSecondMovement(true);
	//xRandom=null;
	//tempX=null;
	}
	listSoftwares=sort.quickSort(listSoftwares);
	}
	
	
	public void createSubSoftwares(){
		int counter = Constant.numOfweakSoftwares;
		for(int i=0;i<Constant.numOfTopSoftware;i++){
			int numOfSubSoftwares =5;
			for(int j=0;j<numOfSubSoftwares;j++){
				double newSoftware[] = new double[Constant.numOfDim];
				for(int k=0;k<Constant.numOfDim;k++){
					double rn1 = rn.nextDouble();
					if(rn1<Constant.a){
						newSoftware[k]=listSoftwares[i].getXSoftware()[k];
					}else{
						int n =(int)((Constant.numOfweakSoftwares)*Math.abs(rn.nextGaussian()));
						if(n>=Constant.numOfweakSoftwares){
							n=Constant.numOfweakSoftwares-1;
						}
						newSoftware[k]=listSoftwares[n].getXSoftware()[k];
					}
				}
				double y=fitness(newSoftware);
				listSoftwares[counter]=new listSoftware(newSoftware,y,1);
				counter++;
			}
		}
		listSoftwares=sort.quickSort(listSoftwares);
	}
	
	public double fitness(double[] dimensions){
		
		double[] y= new double[1];
		
		try {
			fit_cec.test_func(dimensions,y,Constant.numOfDim,1,this.functionName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//y[0]=fit.Griewank(dimensions);
		//y[0]=fit.dejoinNose(dimensions);
		//LessBound = -32.768 , highBound = 32.768
		//y[0] = fit.sphere(dimensions);//LessBound = -10 , highBound = 10
		//y[0]= fit.cosineMixture(dimensions);  //LessBound = -1 , highBound = 1
		//y[0]= fit.Dropwave(dimensions);//LessBound = -5.12 , highBound = 5.12
		//y[0]=fit.Cigar(dimensions);
		//y[0]=fit.Brown3(dimensions);
		//y[0]=fit.rastrigin(dimensions);
		//y[0]=fit.Rosenbrok(dimensions);
		//y[0]=fit.Zakharov_Function(dimensions);
		//y[0]=fit.Stretchedv(dimensions);
//		return y[0];
		
		return bazar.Fitness.fitness(dimensions);
		
	}
	public void print(){
		System.out.print(listSoftwares[0].getySoftware()+",");
		double y = fitness(listSoftwares[0].getXSoftware());
		System.out.println("This : "+ y);
		//System.out.println("itr: "+this.itr+":: "+listSoftwares[0].getySoftware());
		/*for(int i=0;i<Constant.numOfSoftwares;i++){
			System.out.print(listSoftwares[i].getySoftware());
			//System.out.print("x ");
			for(int j=0;j<Constant.numOfDim;j++){
				System.out.print(","+listSoftwares[i].getXSoftware()[j]);
			}
			System.out.println();
			
			}*/
		/*for(int j=0;j<Constant.numOfDim;j++){
			System.out.print("**,");
		}*/
		//System.out.println();
	}
	

}
