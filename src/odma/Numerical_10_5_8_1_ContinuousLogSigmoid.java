package odma;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class Numerical_10_5_8_1_ContinuousLogSigmoid {

	/**
	 * @param args
	 */
	static double input[][] = new double[214][12];
	static double weight[][]= new double[5][11];
	static double secondlayer [][] = new double[214][5];
	static double output[][] = new double [214][1];
//	public static void main(String[] args) throws FileNotFoundException {
//		
////		readfile();
//		double w[]=new double[49];
//		for(int i=0;i<49;++i){
//			w[i]=(double)(Math.random());
//		}
//		setweight(w);
//		makelayer2();
//		makeoutput();
//		calculateERROR();
//		System.out.println();
//		run(w);
//
//	}
	public static double run(double w[]) throws FileNotFoundException{
		readfile();
		setweight(w);
		makelayer2();
		makeoutput();
		double error=calculateERROR();
		return error;
	}
	private static double calculateERROR() {
		double error=0;
		double errors=0;
		for(int k=0;k<214;++k){
			error=input[k][11]-output[k][0];
			errors+=Math.pow(error, 2);
		}
		errors=errors/214;
		return (double)errors;
		
		
	}
	private static void makeoutput() {
		for(int k=0;k<214;++k){
			double sum=0;
			for(int j=0;j<5;++j){
				sum+=weight[4][j]*secondlayer[k][j];
			}
			double sums =(5/(1+(1/Math.pow(Math.E, (double)sum))));
			output[k][0]=(double)sums;
		}
		
	}
	private static void makelayer2() {
		for(int k=0;k<214;++k){
			
		for(int j=0;j<4;++j){
			double sum=0;
		for(int i=0;i<11;++i){
			sum=sum+weight[j][i]*input[k][i];
		}
		double sums =(5/(1+(1/Math.pow(Math.E, (double)sum))));
		secondlayer[k][j]=(double)sums;
		}
		}
		for(int k=0;k<secondlayer.length;++k){
			secondlayer[k][4]=1;
		}
		
	}
	private static void setweight(double w[]) {
		
		for(int i=0;i<4;++i){
			for(int j=0;j<11;++j){
			weight[i][j]=w[j+(i*10)];
			}
		}
		int j=0;
		for(int i=44;i<w.length;++i){
			weight[4][j]=w[i];
			++j;
		}
		
		
		
	}
	private static void readfile() throws FileNotFoundException {
		
		File file =new File("dataset.txt");
		Scanner read = new Scanner(file);
		int k=0;
		while(read.hasNext()){
			String line = read.nextLine();
			String instance[]=line.split(",");
			int i=0;
			int l=10;
			int z=0;
			while(i<instance.length){
				if(i==l){
					input[k][i]=1;	
					--i;
					l=-1;
				}else{
					if(l==-1)
						z=1;
				input[k][i+z]=Float.parseFloat(instance[i]);
				}
				++i;
			}
			++k;
		}
	}

}
