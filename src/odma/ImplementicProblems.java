package odma;

import java.util.Random;

import main.Constant;




public class ImplementicProblems {

	/**
	 * @param args
	 */
	public static double start(double[] dimensions,int functionName){
		double y=0;
		switch(functionName){
		case 31:
			y= Ackley(dimensions);
		case 32:
			y=sphere(dimensions);
		case 33:
			y=Stretchedv(dimensions);
		}
		return y;
	}
	public static double Easom_Function(double[] dimensions){
		double y=0;
		
		y= -(Math.cos(dimensions[0])*Math.cos(dimensions[1]))*Math.exp(-(Math.pow(dimensions[0]-Math.PI, 2)) - (Math.pow(dimensions[1]-Math.PI,2)));
		return y;
	}
	public static  double Booth_Function(double[] dimensions){
		double y=0;
		
		y=y+ Math.pow((dimensions[0] + (2 * dimensions[1]) - 7),2) + Math.pow(((2 * dimensions[0]) + dimensions[1] - 5), 2);
		
		return y;
	} 
	public static double Matyas_Function(double[] dimensions){
		
		double y=0;
		
		y=y+ 0.26*(Math.pow(dimensions[0], 2) + Math.pow(dimensions[1], 2) ) - (0.48 * (dimensions[0] * dimensions[1]));
		
		return y;
	}
	public static double Zakharov_Function(double[] dimensions){
		double y=0;
		double x=0;
		double z=0;
		for(int j=0;j<dimensions.length ;j++){
			y=y+ Math.pow(dimensions[j],2);
		}
		
		for(int j=0;j<dimensions.length;j++){
			x=x+ (0.5 * (j+1) * dimensions[j]);
		}
		
		x=Math.pow(y,2);
		
		for(int j=0;j<dimensions.length;j++){
			z=z+ (0.5 * (j+1) * dimensions[j]);
		}
		
		z=Math.pow(y, 4);
		y=y+x+z;
		return y;
	}
	public static double Rosenbrok(double[] dimensions){
		
	double y=0;
	/**Rosenbrok**/
	for(int i=0;i<dimensions.length-1;i++){
	y = 100*(Math.pow((dimensions[i+1] - (Math.pow(dimensions[i], 2))), 2))
	+ (Math.pow((1 - dimensions[i]), 2)) + y;
    }
	return y;
	
		
	}
	public static double Ackley(double[] dimensions){
		/**Ackly*/
		double z=0;
		double y=0;
		double dobnumOfDim=dimensions.length;
		int numOfDim=dimensions.length;
		for(int i=0;i<numOfDim;i++){
		 
			y= Math.pow(dimensions[i], 2) + y;

		}
		
		y= (1/dobnumOfDim) * y ;
		y=Math.sqrt(y);
		y=(-0.2) * y ;
		y=Math.exp(y);
		y= (-20) * y ;
			
		
		for(int i=0;i<numOfDim;i++){
			 
			z= Math.cos(2*Math.PI * dimensions[i]) + z ;

		}
		
		z = (1/dobnumOfDim) * z ;
		
		z=Math.exp(z);
		
		y=(y + 20) + (Math.exp(1) - z );
		   
		return y;
		
	}
	
	public static double Griewank(double[] dimensions){
		double x=0,y=0,z=1;
		
        for(int i=0;i<=dimensions.length-1;i++){
			
			x = Math.pow(dimensions[i], 2) + x ;
		}
        
		x = (1.0/4000)*x;
		for(int i=0 ;i<=dimensions.length-1;i++){
			double iD=i;
			z= ((Math.cos(dimensions[i]/(Math.sqrt(iD+1))))) * z ;
		}
		z=-z+1;
	
		 y= z+x ;
		 return y;
	}
	
	public static double sumSqare(double[] dimensions){
		double y=0;
		for(int i=1;i<dimensions.length;i++){
			y+=i*(Math.pow(dimensions[i], 2));
		}
		
		return y;
	}
	// -10<x<10  min=0
	public static double DixonPrice(double[] dimension){
		double y=0;
		y=Math.pow((dimension[0]-1), 2);
		
		for(int i=1;i<dimension.length;i++){
			y+=i*Math.pow((2*Math.pow(dimension[i],2) - dimension[i-1]), 2);
			
		}
		
		return y;
	}
	
	
	public static double XinSheYang(double[] dimensions){
		double y=0;
		for(int i=0;i<dimensions.length;i++){
			y+=Math.abs(dimensions[i]);
		}
		
		double t=0;
		
		for(int i=0;i<dimensions.length;i++){
			t+=Math.sin(Math.pow(dimensions[i], 2));
		}
		t=Math.exp(t);
		
		y=y/t;
		
		return y;
	}
	public static double EggHolder(double[] dimensions){
		double y=0;
		double t=0;
		y=-dimensions[0] * Math.sin(Math.sqrt(Math.abs(dimensions[0] - dimensions[1] - 47)));
		t=(dimensions[1] +47) * Math.sin(Math.sqrt(Math.abs((1/2 * dimensions[0]) + dimensions[1] + 47)));
		y=y-t;
		return y;
	}
	//min =0; 0<x<10
	public static double DeflectetCorrugatedSpring(double[] dimensions){
		
		double y=0;
		
		for(int i=0;i<dimensions.length;i++){
			
			y+= Math.pow(dimensions[i] - 5, 2); 
			double t=0;
			for(int j=0;j<dimensions.length;j++){
				t+=Math.pow(dimensions[j] - 5, 2);
			}
			
			t=Math.sqrt(t) * 5;
			t=Math.cos(t);
			
			y=y-t;
			
		}
		
		y=y * 0.1;
		
		return y;
	}
	// -5.12<x<5.12  min=-1
	public static double Dropwave(double[] dimensions){
		 
		double y=0;
		
		for(int i=0;i<dimensions.length;i++){
			y+=Math.pow(dimensions[i], 2);
		}
		y=Math.sqrt(y)*12;
		y=Math.cos(y) +1;
		
		double t=0;
		for(int i=0;i<dimensions.length;i++){
			t+=Math.pow(dimensions[i],2);
		}
		
		t=(t * 0.5) +2;
		
		y=y/t;
		
		return -y;
	}
	//  -10<x<10
	public static double dejoinNose(double[] dimensions){
		double y=0;
		Random rand =new Random();
		for(int i=0;i<dimensions.length;i++){
			y+=Math.pow(dimensions[i], 4) + rand.nextDouble();
		}
		
		return y;
		
	}
	
	public static double Cigar(double[] dimensions){
		double y=0;
		y+=Math.pow(dimensions[0], 2);
		
		double t=0;
		
		for(int i=1;i<dimensions.length;i++){
			t+=Math.pow(dimensions[i],2);
		}
		t=t*100000;
		y=y+t;
		return t;
	}
	
	public static double Brown3(double[] dimensions){
		double y=0;
		
		for(int i=0;i<dimensions.length-1;i++){
			y+=Math.pow(Math.pow(dimensions[i], 2),Math.pow(dimensions[i+1], 2) + 1)
			
			+

			Math.pow(Math.pow(dimensions[i+1],2),Math.pow(dimensions[i],2) + 1);
		}
		return y;
	}
	// -1<x<1   min=-1*(dimension)
	public static double cosineMixture(double[] dimensions){
		double y=0;
		
		for(int i=0;i<dimensions.length;i++){
			y+=Math.cos(5 * dimensions[i] * Math.PI);
		}
		y=y*(-0.1);
		
		double t=0;
		
		for(int i=0;i<dimensions.length;i++){
			
			t+=Math.pow(dimensions[i],2);
		}
		
		y=y+t;
		
		return y;
	}
	public static double Levy(double[] dimensions){
		double[] z=new double[dimensions.length];
		
		//calcuate array z
		
		for(int i=0;i<dimensions.length;i++){
			z[i]=((dimensions[i] - 1)/4) + 1;
		}
		
		double t=0;
		
		for(int i=0;i<dimensions.length;i++){
			t+=Math.pow(z[i] - 1, 2) * (1 + (10 * (Math.pow(Math.sin(Math.PI * z[i] +1),2))));
		}
		t+=Math.pow(Math.sin(Math.PI * z[0]), 2);
		
		t+=Math.pow(z[z.length-1] - 1, 2) * (1 + 10*(Math.pow(Math.sin(2 * Math.PI * z[z.length-1]), 2)));
		 
		return t;
	}
	public static double sphere(double[] dimensions){
		
		double y=0;
		
		  for(int i=0;i<dimensions.length;i++)
		     y+=(Math.pow(dimensions[i], 2));
		  
		  return y;
		 
	}
	
	public static double shubert(double[] dimensions){
		
		double y=1;
		
		  for(int i=0;i<dimensions.length;i++){
			 double x=0;
		     for(int j=1;j<=5;j++){
		    	 x+= j*Math.cos((j+1)*dimensions[i]+j);
		     }
		  y*=x;
		  }
		  return y;
		 
	}
	
	public static double rastrigin(double[] dimensions){
		
		double y=0;
		
		  for(int i=0;i<dimensions.length;i++){
			 y+=(Math.pow(dimensions[i], 2))-(10*Math.cos(2*Math.PI*dimensions[i]));
		  }
		  y+=10*dimensions.length;
		  return y;
		 
	}
	

	public static double SineEnvelope(double[] dimensions){
		double y=0;
		
		for(int i=0;i<dimensions.length-1;i++){
			y+=((Math.pow(Math.sin(Math.sqrt(Math.pow(dimensions[i], 2) + Math.pow(dimensions[i+1], 2)) - 0.5), 2))
			/
			Math.pow((0.001*(Math.pow(dimensions[i+1], 2) + Math.pow(dimensions[i], 2))) +1, 2))
			
			+
			0.5
			;
			
		}
	return -y;
	}
	
	public static double Stretchedv(double[] dimensions){
		double y=0;
		
		for(int i=0;i<dimensions.length - 1;i++){
			double t=Math.pow(dimensions[i], 2) + Math.pow(dimensions[i+1], 2);
			
			y+=(Math.pow(t, 1/4))*(Math.pow(Math.sin(50 * Math.pow(t, 0.1)) + 1, 2));
		}
		
		return y;
	}
	public static double Schwefel(double[] dimensions){
		
		double y=0;
		
	   y+=418.9829*Constant.dobNumOfDim;
	   
	   double t=0;
	   
	   for(int i=0;i<dimensions.length;i++){
		   t+=dimensions[i] * Math.sin(Math.sqrt(Math.abs(dimensions[i])));
	   }
			 
			   y=y-t;
			   return y;
			   
		}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
    
	}

}

