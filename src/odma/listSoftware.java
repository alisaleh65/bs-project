package odma;

import java.util.Random;

import main.Constant;

public class listSoftware {

	//this array show value of each dimension of software
	private double[] xSoftware;
	//value of software 
	private double ySoftware;
	//rank of software between other softwares
	
	public double point2=1;
	//Rate of Progress
	//previous Dimension of software
	private double[] preXSoftware;
	//previous value of software
	private double preYSoftware;
	//show do software have second movement
	private boolean secondmovement=false;
	//Random object
	private Random rn;
	//this atribute shows rang of movement
	private double radiousMovement=2;
	//For linear equation.
	public double t1=Constant.radiousMovement;
	double[] vectorConductor=new double[Constant.numOfDim];
	public  listSoftware(double[] insertItem,double ySoftware,int point){
		this.xSoftware = insertItem;
		this.ySoftware=ySoftware;
		this.point2=point;
		rn=new Random();
	}
	
	public void creatVectorConductor(){
	
		for(int i=0;i<Constant.numOfDim;i++){
			this.vectorConductor[i]=this.xSoftware[i]-this.preXSoftware[i];
			/*System.out.println("This"+xSoftware[i]);
			System.out.println("Pre"+this.preXSoftware[i]);
			System.out.println("Vect"+this.vectorConductor[i]);*/
		}
	}
	public void setPreYSoftware(double y){
		this.preYSoftware=y;
	}
	public double getPreYSoftware(){
		return this.preYSoftware;
	}
	public void setPreXSoftware(double[] x){
		this.preXSoftware=x;
	}
	public double[] getPreXSoftware(){
		return this.preXSoftware;
	}
	public void setXSoftware(double[] x){
		this.xSoftware=x;
	}
	
	public void setPoint(int point){
		this.point2=point;
	}
	public double[] getXSoftware(){
		return this.xSoftware;
	}
	public void setYSoftware(double y){
		this.ySoftware=y;
	}
	public double getySoftware(){
		return this.ySoftware;
	}
	
	public void setSecondMovement(boolean b){
		this.secondmovement=b;
	}
	public boolean getSecondMovement(){
		return this.secondmovement;
	}
	public void setRadiosMovement(double rm){
		this.radiousMovement=rm;
	}
	public double getRadiousMovement(){
		return this.radiousMovement;
	}

	public double[] creatLinerEquation(int itr) {
		// TODO Auto-generated method stub
		double[] tempX=new double[Constant.numOfDim];
		double t2=t1/10;
		//double t=t1 + (rn.nextDouble()*t2);
		double itr2=5*itr/Constant.iterations;
		double normal =Constant.highBound*(0.2+((1/(Math.sqrt(5)*Math.sqrt(2*Math.PI)))*Math.exp(-(Math.pow(itr2-0, 2)/2*5))));
		double t=1+rn.nextDouble() *(normal);
		//double Radian=((rn.nextDouble())*(Math.PI/4));
		//System.out.println("X new");
		for(int i=0;i<Constant.numOfDim;i++){
			double newX=((this.vectorConductor[i]*t) + this.xSoftware[i]);
			if(newX>=Constant.lessBound && newX<=Constant.highBound){
			tempX[i]=newX;
			}else if(newX<=Constant.lessBound){
				tempX[i]=Constant.lessBound;
			}else{
				tempX[i]=Constant.highBound;
			}
		//System.out.print(tempX[i]+";");	
		}
		t1=t2;
		return tempX;
	}
}
